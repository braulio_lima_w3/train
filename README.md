# Ferramentas

- Java 8 (OpenSDK 1.8)
- Spring Boot 2
- JPA
- Hibernate
- MySQL
- Intellij IDEA
- Git
- Maven

# Para baixar o projeto já configurado

    git clone -b empty-projects https://bitbucket.org/braulio_lima_w3/train.git

# Leitura Adicional

- [Spring-Data repository](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.core-concepts)