
package com.neppo.estagio.view.services;

import com.neppo.estagio.core.models.Usuario;

import java.util.List;

public interface UsuarioService {

    Usuario getById(Long id);

    List<Usuario> getAll();

    Usuario save(Usuario person);

    List<Usuario> getByName(String name);

    List<Usuario> getByNameContaining(String name);

}
