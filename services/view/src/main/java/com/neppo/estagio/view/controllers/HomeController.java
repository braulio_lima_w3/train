package com.neppo.estagio.view.controllers;

import com.neppo.estagio.core.models.Usuario;
import com.neppo.estagio.view.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;


@RestController
@RequestMapping("/")
public class HomeController {

    private UsuarioService usuarioService;

    @Autowired
    public HomeController(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    @GetMapping()
    private ResponseEntity<Object> index(){

        Usuario p = new Usuario();

        p.setData_nascimento(new Date());
        p.setNome("Hello World");


        p = usuarioService.save(p);


        Usuario g = new Usuario();

        g.setData_nascimento(new Date());
        g.setNome("ggez");

        g = usuarioService.save(g);


        return ResponseEntity.ok(new Object[]{ p , usuarioService.getByNameContaining("e") });
    }

}