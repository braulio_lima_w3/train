package com.neppo.estagio.view;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan("com.neppo.estagio.core.models")
@EnableJpaRepositories("com.neppo.estagio.core.repositories")
public class TreinamentoApplication {

	public static void main(String[] args) {

		SpringApplication.run(TreinamentoApplication.class, args);
	}
}
