package com.neppo.estagio.core.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;


@Entity
@Table(name = "HR_RESPOSTA")
public class Resposta implements Serializable {
    @Id
    @OneToOne
    @JoinColumn(name = "FK_Pergunta_ID")
    private Pergunta pergunta;

    @Id
    @OneToOne
    @JoinColumn(name = "FK_Usuario_ID")
    private Usuario usuario;


    @Column
    private String resposta;

    @Column
    private long tentativa;

    public Pergunta getPergunta() {
        return pergunta;
    }

    public void setPergunta(Pergunta pergunta) {
        this.pergunta = pergunta;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    public long getTentativa() {
        return tentativa;
    }

    public void setTentativa(long tentativa) {
        this.tentativa = tentativa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Resposta resposta1 = (Resposta) o;
        return tentativa == resposta1.tentativa &&
                Objects.equals(pergunta, resposta1.pergunta) &&
                Objects.equals(usuario, resposta1.usuario) &&
                Objects.equals(resposta, resposta1.resposta);
    }

    @Override
    public int hashCode() {

        return Objects.hash(pergunta, usuario, resposta, tentativa);
    }
}
