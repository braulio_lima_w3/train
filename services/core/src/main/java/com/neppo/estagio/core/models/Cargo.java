package com.neppo.estagio.core.models;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "HR_CARGO")
public class Cargo {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "FK_Departamento_ID")
    private Departamento departamento_id;

    @ManyToMany
    @JoinTable(name = "HR_USUARIO_CARGO",
            joinColumns = @JoinColumn(name = "FK_Cargo_ID",
                    referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "FK_Usuario_ID",
                    referencedColumnName = "id")
    )
    private List<Usuario> usuarios;

    @ManyToMany
    @JoinTable(name = "HR_CARGO_COMPETENCIA",
            joinColumns = @JoinColumn(name = "FK_Cargo_ID",
                    referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "FK_Competencia_ID",
                    referencedColumnName = "id") )
    private  List<Competencia> competencias;

    @Column
    private String nome;

    @Column
    private String descricao;

    @Column
    private String status;

    @Column
    private String created_by;

    @Column
    private Date created_at;

    @Column
    private String updated_by;

    @Column
    private String updated_at;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Departamento getDepartamento_id() {
        return departamento_id;
    }

    public void setDepartamento_id(Departamento departamento_id) {
        this.departamento_id = departamento_id;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public List<Competencia> getCompetencias() {
        return competencias;
    }

    public void setCompetencias(List<Competencia> competencias) {
        this.competencias = competencias;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cargo cargo = (Cargo) o;
        return id == cargo.id &&
                Objects.equals(departamento_id, cargo.departamento_id) &&
                Objects.equals(usuarios, cargo.usuarios) &&
                Objects.equals(competencias, cargo.competencias) &&
                Objects.equals(nome, cargo.nome) &&
                Objects.equals(descricao, cargo.descricao) &&
                Objects.equals(status, cargo.status) &&
                Objects.equals(created_by, cargo.created_by) &&
                Objects.equals(created_at, cargo.created_at) &&
                Objects.equals(updated_by, cargo.updated_by) &&
                Objects.equals(updated_at, cargo.updated_at);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, departamento_id, usuarios, competencias, nome, descricao, status, created_by, created_at, updated_by, updated_at);
    }
}
