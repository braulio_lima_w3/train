package com.neppo.estagio.core.models;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "HR_ALTERNATIVA")
public class Alternativa {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String conteudo;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getConteudo() {
        return conteudo;
    }

    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Alternativa that = (Alternativa) o;
        return id == that.id &&
                Objects.equals(conteudo, that.conteudo);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, conteudo);
    }
}
