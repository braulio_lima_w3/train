package com.neppo.estagio.core.models;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "HR_USUARIO")
public class Usuario {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String nome;

    @Column
    private String CPF_CNPJ;

    @Column
    private String status;

    @Column
    private Date data_nascimento;

    @Column
    private String email;

    @Column
    private String login;

    @Column
    private long senha;

    @Column
    private String created_by;

    @Column
    private Date created_at;

    @Column
    private String updated_by;

    @Column
    private String updated_at;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCPF_CNPJ() {
        return CPF_CNPJ;
    }

    public void setCPF_CNPJ(String CPF_CNPJ) {
        this.CPF_CNPJ = CPF_CNPJ;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getData_nascimento() {
        return data_nascimento;
    }

    public void setData_nascimento(Date data_nascimento) {
        this.data_nascimento = data_nascimento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public long getSenha() {
        return senha;
    }

    public void setSenha(long senha) {
        this.senha = senha;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return id == usuario.id &&
                senha == usuario.senha &&
                Objects.equals(nome, usuario.nome) &&
                Objects.equals(CPF_CNPJ, usuario.CPF_CNPJ) &&
                Objects.equals(status, usuario.status) &&
                Objects.equals(data_nascimento, usuario.data_nascimento) &&
                Objects.equals(email, usuario.email) &&
                Objects.equals(login, usuario.login) &&
                Objects.equals(created_by, usuario.created_by) &&
                Objects.equals(created_at, usuario.created_at) &&
                Objects.equals(updated_by, usuario.updated_by) &&
                Objects.equals(updated_at, usuario.updated_at);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, nome, CPF_CNPJ, status, data_nascimento, email, login, senha, created_by, created_at, updated_by, updated_at);
    }
}
