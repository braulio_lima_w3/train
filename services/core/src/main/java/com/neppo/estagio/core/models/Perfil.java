package com.neppo.estagio.core.models;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "HR_PERFIL")
public class Perfil {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String nome;

    @Column
    private String descricao;

    @Column
    private String status;

    @Column
    private long peso;

    @Column
    private String created_by;

    @Column
    private Date created_at;

    @Column
    private String updated_by;

    @Column
    private String updated_at;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getPeso() {
        return peso;
    }

    public void setPeso(long peso) {
        this.peso = peso;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Perfil perfil = (Perfil) o;
        return id == perfil.id &&
                peso == perfil.peso &&
                Objects.equals(nome, perfil.nome) &&
                Objects.equals(descricao, perfil.descricao) &&
                Objects.equals(status, perfil.status) &&
                Objects.equals(created_by, perfil.created_by) &&
                Objects.equals(created_at, perfil.created_at) &&
                Objects.equals(updated_by, perfil.updated_by) &&
                Objects.equals(updated_at, perfil.updated_at);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, nome, descricao, status, peso, created_by, created_at, updated_by, updated_at);
    }
}
