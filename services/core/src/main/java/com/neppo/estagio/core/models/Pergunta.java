package com.neppo.estagio.core.models;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "HR_PERGUNTA")
public class Pergunta {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne
    @JoinColumn(name = "FK_Formulario_ID")
    private Formulario formulario_id;

    @ManyToMany
    @JoinTable(
            name = "HR_PERGUNTA_ALTERNATIVA",

            joinColumns = @JoinColumn(
                    name = "FK_Pergunta_ID",
                    referencedColumnName = "id"),

            inverseJoinColumns = @JoinColumn(
                    name = "FK_Alternativa_ID",
                    referencedColumnName = "id")
    )
    private List<Alternativa> alternativas;

    @Column
    private String titulo;

    @Column
    private String tipo_pergunta;

    @Column
    private String created_by;

    @Column
    private Date created_at;

    @Column
    private String updated_by;

    @Column
    private String updated_at;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Formulario getFormulario_id() {
        return formulario_id;
    }

    public void setFormulario_id(Formulario formulario_id) {
        this.formulario_id = formulario_id;
    }

    public List<Alternativa> getAlternativas() {
        return alternativas;
    }

    public void setAlternativas(List<Alternativa> alternativas) {
        this.alternativas = alternativas;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTipo_pergunta() {
        return tipo_pergunta;
    }

    public void setTipo_pergunta(String tipo_pergunta) {
        this.tipo_pergunta = tipo_pergunta;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pergunta pergunta = (Pergunta) o;
        return id == pergunta.id &&
                Objects.equals(formulario_id, pergunta.formulario_id) &&
                Objects.equals(alternativas, pergunta.alternativas) &&
                Objects.equals(titulo, pergunta.titulo) &&
                Objects.equals(tipo_pergunta, pergunta.tipo_pergunta) &&
                Objects.equals(created_by, pergunta.created_by) &&
                Objects.equals(created_at, pergunta.created_at) &&
                Objects.equals(updated_by, pergunta.updated_by) &&
                Objects.equals(updated_at, pergunta.updated_at);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, formulario_id, alternativas, titulo, tipo_pergunta, created_by, created_at, updated_by, updated_at);
    }
}
