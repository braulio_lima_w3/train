package com.neppo.estagio.core.repositories;

import com.neppo.estagio.core.models.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    List<Usuario> findByNome(String name);

    @Query(value = "select p from Usuario p where upper(p.nome) like upper(?1)")
    List<Usuario> findByNameHQL(String name);

    @Query(value = "select * from HR_USUARIO where lower(nome) like lower(?1)", nativeQuery = true)
    List<Usuario> findByNameRaw(String name);

    List<Usuario> findByNomeContainsIgnoreCase(String name);

}

/*
 * nao sao implementados pelo usuario de forma direta.
 *
 * 3 formas de realizar queries no banco
 *
 * - pelo nome da funcao
 * - pelo @Query - pelo HQL
 * - pelo @Query(nativeQuery=true) - via o sql do proprio SGDB
 *
 * */