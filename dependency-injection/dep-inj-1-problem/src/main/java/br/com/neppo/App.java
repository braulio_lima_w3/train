package br.com.neppo;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        // criar instancia de um knight
        Knight k = new Knight();

        // criar uma instancia com uma acoplacao forte do knight.
        Level l = new Level(k);

        // imprimir localizacao atual
        System.out.println(l.currentPlayerLocation());
    }
}
/*
 * Obviamente esse codigo pode ser melhorado.
 *
 * Como?
 *
 * Proximo projeto...
 *
 * */
