package br.com.neppo;

public class Level {

    private Knight knight;

    Level(Knight knight){
        this.knight = knight;
    }

    public String currentPlayerLocation(){
        return String.format("( %d , %d )", knight.posX(), knight.posY());
    }
}
