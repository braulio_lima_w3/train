package br.com.neppo;

public class Level {

    private Playable playable;

    Level(Playable playable){
        this.playable = playable;
    }

    public String currentPlayerLocation(){
        return String.format("( %d , %d )", playable.posX(), playable.posY());
    }

    public String ForceMovement (){
        playable.walk(-5, 5);
        return String.format("( %d , %d )", playable.posX(), playable.posY());
    }

    public void changePlayable(Playable playable){
        this.playable = playable;
    }

}
