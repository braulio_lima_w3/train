package br.com.neppo;

public class LevelManager {

    private static Level l;
    private static Playable p;

    public static Level changePlayable(String context){
        if("paladin".equals(context)){
            p = new Paladin();
        }
        else{
            p = new Knight();
        }

        if(l == null){
            l = new Level(p);
        }
        else {
            l.changePlayable(p);
        }

        return l;
    }

}
