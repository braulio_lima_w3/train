package br.com.neppo;


import java.util.Vector;

public interface Playable {
    int posX();
    int posY();
    int[] walk(int x, int y);
}
