package br.com.neppo;

public class Paladin implements Playable {

    private int x = 0;
    private int y = 0;


    public int posX() {
        return x;
    }

    public int posY() {
        return y;
    }

    public int[] walk(int x, int y) {

        this.x += Math.floor(x / 2);
        this.y += Math.floor(y / 2);

        return new int[] { this.x, this.y };
    }
}
