package br.com.neppo;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class KnightTest {

    Knight k;

    @Before
    public void init(){
        this.k = new Knight();
    }

    @Test
    public void moveTest(){
        int[] pos = k.walk(2,2);

        assertEquals(pos.length, 2);

        assertEquals(k.posX(), 12);
        assertEquals(k.posY(), 12);
    }

}
