package br.com.neppo;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class MockLevelTest {

    @Mock
    private Playable p;

    private Level l;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        this.l = new Level(this.p);
    }

    @Test
    public void testForceMovement(){

        int x = 92;
        int y = 12;
        int pos[] = new int[]{ x, y };

        when(this.p.walk(-5, 5)).thenReturn(pos);
        when(this.p.posX()).thenReturn(x);
        when(this.p.posY()).thenReturn(y);

        String output = this.l.ForceMovement();

        assertEquals(output, "( 92 , 12 )");

        verify(this.p, times(1)).walk(-5, 5);
        verify(this.p, times(1)).posX();
        verify(this.p, times(1)).posY();

        verifyNoMoreInteractions(this.p);

    }


}
