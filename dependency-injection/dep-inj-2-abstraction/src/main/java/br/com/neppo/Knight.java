package br.com.neppo;

import java.util.Vector;

public class Knight implements Playable{
    private int x = 10;
    private int y = 10;

    public int posX(){
        return x;
    }

    public int posY(){
        return y;
    }

    public int[] walk(int x, int y) {
        this.x += x;
        this.y += y;

        int[] rtn = new int[2];

        rtn[0] = x;
        rtn[1] = y;

        return rtn;
    }
}
