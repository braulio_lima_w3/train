package br.com.neppo;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        // criar instancia de um knight
        Knight k = new Knight();
        Paladin p = new Paladin();

        // criar um nivel novo
        Level l = new Level(k);
        l.ForceMovement();
        k.walk(2,2);
        System.out.println(l.currentPlayerLocation());


        // trocar o player do nivel
        l.changePlayable(p);
        l.ForceMovement();
        p.walk(2,2);
        System.out.println(l.currentPlayerLocation());
    }
}
/*
 * Obviamente esse codigo pode ser melhorado.
 *
 * Como?
 *
 * Proximo projeto...
 *
 * */
